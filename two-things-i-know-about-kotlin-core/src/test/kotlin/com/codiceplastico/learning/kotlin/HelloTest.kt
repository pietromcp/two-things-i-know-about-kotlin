package com.codiceplastico.learning.kotlin

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class HelloTest {
    @Test
    fun sayHelloWorld() {
        assertThat(Hello().sayHello(), `is`(equalTo("Hello, World!")))
    }
}